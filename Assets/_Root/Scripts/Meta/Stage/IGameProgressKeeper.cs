namespace TestBlast.Meta
{
    public interface IGameProgressKeeper
    {
        int CurrentLevel { get; }

        void CompleteCurrentLevel();
    }

    class GameProgressKeeper : IGameProgressKeeper
    {
        public int CurrentLevel => currentLevel;
        
        public void CompleteCurrentLevel()
        {
            currentLevel++;
        }

        private int currentLevel = 1;
    }
}