namespace TestBlast.Meta
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Config;
    using Core.Field;
    using Core.Field.Blocks;
    using Core.Field.Generators;

    public class GameStage
    {
        public GameField Field { get; }

        public int MovesLimit { get;  }

        public int MovesPast { get; private set; }

        public List<StageTask> Tasks { get; }

        public event Action MovesLimitReached;
        
        public event Action AllTasksComplete;
        
        public event Action<int> MovePerformed;
        
        private IFieldGenerator generator;

        private bool alreadyComplete;
        
        public GameStage(GameField gameField, List<StageTask> tasks, int moves)
        {
            Tasks = tasks;
            Field = gameField;
            MovesLimit = moves;
            
            Field.MovePerformed += OnMovePerformed;

            foreach (var task in tasks)
            {
                task.SubscribeTo(this);
                task.TaskUpdated += OnTaskUpdated;
            }
        }

        private void OnTaskUpdated()
        {
            if (Tasks.All(task => task.Completed) && !alreadyComplete)
            {
                alreadyComplete = true;
                AllTasksComplete?.Invoke();
            }
        }

        private void OnMovePerformed()
        {
            MovesPast++;
            MovePerformed?.Invoke(MovesLimit - MovesPast);
            if (MovesPast == MovesLimit && Tasks.Any(task => !task.Completed))
            {
                MovesLimitReached?.Invoke();
            }
        }
    }
}