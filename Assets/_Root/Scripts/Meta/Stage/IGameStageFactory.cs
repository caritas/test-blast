namespace TestBlast.Meta
{
    using System.Collections.Generic;
    using System.Linq;
    using Config;
    using Core.Field;
    using Core.Field.Generators;
    using Core.Field.GroupProcessors;
    using UnityEngine;

    public interface IGameStageFactory
    {
        GameStage PrepareStage(GameStageConfig config, IEnumerable<GameFeature> features);
    }

    public class GameStageFactory : IGameStageFactory
    {
        public GameStage PrepareStage(GameStageConfig config, IEnumerable<GameFeature> features)
        {
            var (width, height) = config.FieldSize;
            var seed = config.Seed ?? Random.Range(0, int.MaxValue);
            var processors = new List<IBlockGroupProcessor>();

            if (features.Contains(GameFeature.RowColumnDestroyers))
            {
                processors.Add(new RowColumnDestroyerProcessor());
            }
            
            var generator = new RandomSimpleBlocksGenerator(width, height,
                config.BlockWeights.Keys.ToList(), seed, processors);
            
            var field = new GameField(generator);
            var tasks = config.Tasks.Select(pair => new CollectSimpleBlocksTask(pair.Key, pair.Value))
                .Cast<StageTask>().ToList();
            
            return new GameStage(field, tasks, config.MovesLimit);
        }
    }
}