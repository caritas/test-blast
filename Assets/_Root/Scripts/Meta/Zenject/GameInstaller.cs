namespace TestBlast.Meta
{
    using Config;
    using Zenject;

    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameProgressKeeper>().To<GameProgressKeeper>().AsSingle();
            Container.Bind<IGameStageFactory>().To<GameStageFactory>().AsSingle();
            Container.Bind(typeof(IStageConfigProvider), typeof(IAvailableFeaturesProvider))
                .To<StageConfigProvider>().AsSingle();
        }
    }
}

