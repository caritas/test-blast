namespace TestBlast.Meta.Loop
{
    using System;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class GameSceneLoader : MonoBehaviour
    {
        private void Start()
        {
            SceneManager.LoadScene("Game");
        }
    }
}