namespace TestBlast.Meta.Config
{
    using System;
    using System.Collections.Generic;
    using Core.Field.Blocks;

    public interface IStageConfigProvider
    {
        GameStageConfig GetConfigFor(int level);
    }

    /// <summary>
    /// Configs can be stored in text format for easy downloading from server and\or A\B tests,
    /// or can be set up in ScriptableObject inside unity - all those formats can be easily converted
    /// to GameStageConfig class used in app.
    ///
    /// In this example, few level configs are just hardcoded
    /// </summary>
    class StageConfigProvider : IStageConfigProvider, IAvailableFeaturesProvider
    {
        private readonly List<GameStageConfig> configs = new List<GameStageConfig>()
        {
            new GameStageConfig
            {
                StageId = "S01",
                FieldSize = (8,8),
                MovesLimit = 30,
                BlockWeights = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Blue] = 1,
                    [SimpleBlockColor.Green] = 2,
                    [SimpleBlockColor.Red] = 3,
                },
                Tasks = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Red] = 10,
                    [SimpleBlockColor.Green] = 5,
                    [SimpleBlockColor.Blue] = 20,
                }
            },
            new GameStageConfig
            {
                StageId = "S02",
                FieldSize = (7,7),
                MovesLimit = 25,
                BlockWeights = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Orange] = 4,
                    [SimpleBlockColor.Yellow] = 1,
                    [SimpleBlockColor.Purple] = 3,
                    [SimpleBlockColor.Green] = 3,
                },
                Tasks = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Orange] = 15,
                    [SimpleBlockColor.Green] = 5,
                    [SimpleBlockColor.Purple] = 25,
                }
            },
            new GameStageConfig
            {
                StageId = "S03",
                FieldSize = (9,8),
                MovesLimit = 20,
                BlockWeights = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Blue] = 1,
                    [SimpleBlockColor.Green] = 2,
                    [SimpleBlockColor.Purple] = 3,
                    [SimpleBlockColor.Yellow] = 2,
                    [SimpleBlockColor.Orange] = 3,
                },
                Tasks = new Dictionary<SimpleBlockColor, int>()
                {
                    [SimpleBlockColor.Purple] = 10,
                    [SimpleBlockColor.Green] = 25,
                    [SimpleBlockColor.Blue] = 20,
                }
            }
        };
        
        public GameStageConfig GetConfigFor(int level)
        {
            return configs[(level - 1) % configs.Count];
        }

        public IEnumerable<GameFeature> GetAvailableFeaturesAtLevel(int level)
        {
            if (level >= 2)
            {
                yield return GameFeature.RowColumnDestroyers;
            }
        }
    }
}