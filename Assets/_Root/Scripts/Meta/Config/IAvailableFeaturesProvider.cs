namespace TestBlast.Meta.Config
{
    using System.Collections.Generic;

    public interface IAvailableFeaturesProvider
    {
        IEnumerable<GameFeature> GetAvailableFeaturesAtLevel(int level);
    }
    
    
    public enum GameFeature
    {
        RowColumnDestroyers
    }
}