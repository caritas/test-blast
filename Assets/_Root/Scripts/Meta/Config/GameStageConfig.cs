namespace TestBlast.Meta.Config
{
    using System.Collections.Generic;
    using Core.Field.Blocks;

    public class GameStageConfig
    {
        public string StageId { get; set; }
        
        /// <summary>
        /// Supported block types and their field appearance probabilities 
        /// </summary>
        public IDictionary<SimpleBlockColor, int> BlockWeights { get; set; }
        
        /// <summary>
        /// Currently, only square fields are supported, so (width, height) is expected
        /// </summary>
        public (int, int) FieldSize { get; set; }
        
        /// <summary>
        /// Currently, only 'Collect N Simple Blocks' tasks are supported, so this is simple config format
        /// 'type' - 'number to collect' 
        /// </summary>
        public IDictionary<SimpleBlockColor, int> Tasks { get; set; }
        
        public int MovesLimit { get; set; }
        
        /// <summary>
        /// Field seed, random if not specified
        /// </summary>
        public int? Seed { get; set; }
    }

}