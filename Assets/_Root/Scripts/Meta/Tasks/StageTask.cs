namespace TestBlast.Meta
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Field.Blocks;

    public abstract class StageTask
    {
        public bool Completed { get; protected set; }

        public event Action TaskUpdated;
        
        public abstract void SubscribeTo(GameStage stage);

        protected void NotifyTaskUpdated()
        {
            TaskUpdated?.Invoke();
        }
    }

    public class CollectSimpleBlocksTask : StageTask
    {
        public SimpleBlockColor Color { get; private set; }

        public int Amount { get; private set; }

        public CollectSimpleBlocksTask(SimpleBlockColor color, int amount)
        {
            Color = color;
            Amount = amount;
        }

        public override void SubscribeTo(GameStage stage)
        {
            stage.Field.BlocksCleaned += OnBlocksCleaned;
        }

        private void OnBlocksCleaned(List<Block> blocks)
        {
            var count = blocks.OfType<SimpleBlock>().Count(block => block.Color == Color);
            if (count == 0)
            {
                return;
            }

            Amount = Math.Max(Amount - count, 0);
            Completed = Amount == 0;
            NotifyTaskUpdated();
        }
    }
}