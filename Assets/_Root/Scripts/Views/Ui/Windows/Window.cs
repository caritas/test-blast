namespace TestBlast.Views.Ui.Windows
{
    using UnityEngine;

    public class Window : MonoBehaviour
    {
        public void Close()
        {
            Destroy(gameObject);
        }
    }
}