namespace TestBlast.Views.Ui.Windows.Types
{
    using System;
    using JetBrains.Annotations;

    public class HomeWindow : Window
    {
        public event Action PlayRequested;

        [UsedImplicitly]
        public void Play()
        {
            Close();
            PlayRequested?.Invoke();
        }
    }
}