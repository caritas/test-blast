namespace TestBlast.Views.Ui.Windows.Types
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    public class LevelEndWindow : Window
    {
        [SerializeField]
        private Button play;
        
        [SerializeField]
        private Button returnHome;
        
        public void Init(Action returnHomeAction, Action continuePlayAction)
        {
            returnHome.onClick.AddListener(() =>
            {
                Close();
                returnHomeAction?.Invoke();
            });
            
            play.onClick.AddListener(() =>
            {
                Close();
                continuePlayAction?.Invoke();
            });
        }
    }
}