namespace TestBlast.Views.Ui.Windows
{
    using UnityEngine;

    public class WindowsBuilder : MonoBehaviour
    {
        public T CreateWindow<T>() where T : Window
        {
            var prefab = Resources.Load<T>($"Windows/{typeof(T).Name}");
            var window = Instantiate(prefab, transform);
            return window;
        }
    }
}