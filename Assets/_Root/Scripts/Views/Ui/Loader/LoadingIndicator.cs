namespace TestBlast.Views.Ui.Loader
{
    using System;
    using System.Collections;
    using TMPro;
    using UnityEngine;

    public class LoadingIndicator : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text dots;

        [SerializeField, Range(0.1f, 1)]
        private float delay = 0.3f;           

        private void Start()
        {
            StartCoroutine(DotsRoutine());
        }

        private IEnumerator DotsRoutine()
        {
            while (this)
            {
                dots.text = "";
                for (var i = 0; i < 4; i++)
                {
                    yield return new WaitForSeconds(delay);
                    dots.text += ".";
                }
            }
        }
    }
}