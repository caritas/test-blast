namespace TestBlast.Views.Ui
{
    using System;
    using Meta;
    using TMPro;
    using UnityEngine;
    using Zenject;

    [RequireComponent(typeof(TMP_Text))]
    public class LevelIndicator : MonoBehaviour
    {
        [Inject]
        public IGameProgressKeeper ProgressKeeper { get; set; }

        private void Start()
        {
            var text = GetComponent<TMP_Text>();
            text.SetText(string.Format(text.text, ProgressKeeper.CurrentLevel));
        }
    }
}