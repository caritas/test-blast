namespace TestBlast.Views.Ui
{
    using Meta;
    using TMPro;
    using UnityEngine;

    public class MovesIndicator : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text movesLeft;

        public void SubscribeTo(GameStage stage)
        {
            stage.MovePerformed += OnMovePerformed;
            movesLeft.SetText(stage.MovesLimit.ToString());
        }

        private void OnMovePerformed(int movesLeftAmount)
        {
            movesLeft.SetText(movesLeftAmount.ToString());
        }
    }
}