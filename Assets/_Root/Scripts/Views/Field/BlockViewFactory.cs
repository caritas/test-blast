namespace TestBlast.Views.Field
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Field.Blocks;
    using UnityEngine;

    public class BlockViewFactory : MonoBehaviour
    {
        [SerializeField]
        private List<SimpleBlockPrefabData> simpleBlocks;
        
        [SerializeField]
        private BlockView rowCleanerBlock;
        
        [SerializeField]
        private BlockView columnCleanerBlock;

        public BlockView GetBlockViewPrefabFor(Block block)
        {
            switch (block)
            {
                case SimpleBlock simpleBlock:
                    return simpleBlocks.FirstOrDefault(data => data.Color == simpleBlock.Color)?.Prefab;
                case RowCleanerBlock _:
                    return rowCleanerBlock;
                case ColumnCleanerBlock _:
                    return columnCleanerBlock;
                default:
                    return null;
            }
        }
    }

    [Serializable]
    public class SimpleBlockPrefabData
    {
        public SimpleBlockColor Color;

        public BlockView Prefab;
    }
    
    
}