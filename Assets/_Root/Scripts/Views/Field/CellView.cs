namespace TestBlast.Views.Field
{
    using System;
    using System.Threading.Tasks;
    using Core.Field;
    using DG.Tweening;
    using UnityEngine;

    public class CellView : MonoBehaviour, ICellView
    {
        private BlockView blockView;
        
        public Position Position { get; private set; }

        public void SetPosition(Position position)
        {
            Position = position;
        }
        
        public void PlaceBlock(BlockView view)
        {
            blockView = view;
        }
        
        
        public void PlaceFallenBlock(BlockView view)
        {
            blockView = view;
            
            blockView.transform.DOLocalMoveY(800, 1200).From().SetSpeedBased().SetEase(Ease.OutSine);
        }

        public void TakeBlockFrom(CellView cellView)
        {
            blockView = cellView.blockView;
            cellView.blockView = null;
            
            blockView.transform.SetParent(transform, true);
            blockView.transform.DOLocalMove(Vector3.zero, 800).SetSpeedBased().SetEase(Ease.OutSine);
        }
        
        public Task DestroyBlock()
        {
            Destroy(blockView.gameObject);
            return Task.CompletedTask;
        }

        public Task MoveBlockTo(CellView goalView)
        {
            var duration = 0.2f;
            blockView.transform.DOMove(goalView.transform.position, duration);
            return Task.Delay(TimeSpan.FromSeconds(duration));
        }
    }
}