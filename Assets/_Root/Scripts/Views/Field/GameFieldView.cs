namespace TestBlast.Views.Field
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Core.Field;
    using Core.Field.Blocks;
    using DG.Tweening;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class GameFieldView : MonoBehaviour, IFieldView, IPointerClickHandler
    {
        [SerializeField, Header("Cells")]
        private CellView cellPrefab;

        [SerializeField, Range(30, 120)]
        private float cellSize = 50;
        
        [SerializeField, Header("Content")]
        private RectTransform fieldBack;
        
        [SerializeField]
        private RectTransform topHud;

        [SerializeField]
        private RectTransform bottomHud;

        [SerializeField, Header("Blocks")]
        private BlockViewFactory blockViewFactory;

        private Vector2 fieldOffset;

        private GameField field;

        private List<CellView> cells = new List<CellView>();

        private CellView GetViewFor(Cell cell)
        {
            return cells.First(view => view.Position == cell.Position);
        }

        public void InitWith(GameField field)
        {
            this.field = field;
            field.SetView(this);
            
            fieldBack.sizeDelta = new Vector2(field.Width, field.Height) * cellSize + Vector2.one * 50;
            
            foreach (var cell in field.AllCells)
            {
                var pos = cell.Position;
                var cellView = Instantiate(cellPrefab, transform);
                cell.InitView(cellView);
                cellView.SetPosition(pos);
                cellView.name = $"Cell {cell.Position}";
                cells.Add(cellView);
                var rt = cellView.GetComponent<RectTransform>();
                fieldOffset = new Vector2(1 - field.Width, field.Height - 1) * cellSize * 0.5f;
                rt.anchoredPosition = new Vector2(pos.Column * cellSize, -pos.Row * cellSize) + fieldOffset;
                rt.sizeDelta = Vector2.one * cellSize;

                if (cell.IsEmpty)
                {
                    continue;
                }

                SpawnBlockIn(cell);
            }

            //little animation
            foreach (var view in cells)
            {
                view.transform.DOScale(Vector3.zero, 0.2f).From()
                    .SetEase(Ease.OutBack)
                    .SetDelay(Random.value * 0.5f + 0.1f);
            }

            topHud.DOMoveY(500, 0.4f).From(true);
            bottomHud.DOMoveY(-300, 0.4f).From(true);
        }

        private void SpawnBlockIn(Cell cell)
        {
            var cellView = GetViewFor(cell);
            var blockPrefab = blockViewFactory.GetBlockViewPrefabFor(cell.Block);
            cellView.PlaceBlock(Instantiate(blockPrefab, cellView.transform));
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //We are reducing components complexity by avoiding separate colliders on each cell/token - in this example
            //we can handle click on cell here

            var screenPos = eventData.position;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), screenPos, null,
                out var point);

            point = (point - fieldOffset + new Vector2(cellSize, -cellSize) * 0.5f) / cellSize;
            var clickedPosition = new Position(-(int) point.y, (int) point.x);

            field.TryActivateCell(clickedPosition);
        }

        public async Task MoveBlock(Cell from, Cell to)
        {
            GetViewFor(to).TakeBlockFrom(GetViewFor(from));
        }

        public void PeekFallenBlock(Cell cell, Block block)
        {
            var cellView = cells.First(view => view.Position == cell.Position);
            
            var blockPrefab = blockViewFactory.GetBlockViewPrefabFor(cell.Block);
            cellView.PlaceFallenBlock(Instantiate(blockPrefab, cellView.transform));
        }

        public async Task CollectBlocksAndConvert(List<Cell> toCollect, Cell clickedCell)
        {
            var views = toCollect.Select(GetViewFor).ToList();
            var goalView = GetViewFor(clickedCell);
            await Task.WhenAll(views.Select(view => view.MoveBlockTo(goalView)));
            await goalView.DestroyBlock();
            await Task.WhenAll(views.Select(view => view.DestroyBlock()));

            SpawnBlockIn(clickedCell);
        }
    }
}