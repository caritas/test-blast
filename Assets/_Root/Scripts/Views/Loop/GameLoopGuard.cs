namespace TestBlast.Meta.Loop
{
    using Config;
    using UnityEngine;
    using Views;
    using Views.Ui.Windows;
    using Views.Ui.Windows.Types;
    using Zenject;

    public class GameLoopGuard : MonoBehaviour
    {
        [SerializeField, Header("Prefabs")]
        private GameStageView stageViewPrefab;

        [SerializeField, Header("Parents")]
        private Transform stageParent;

        [SerializeField]
        private WindowsBuilder windowsBuilder;

        public GameLoopGuard(GameStageView stageViewPrefab, Transform stageParent, WindowsBuilder windowsBuilder)
        {
            this.stageViewPrefab = stageViewPrefab;
            this.stageParent = stageParent;
            this.windowsBuilder = windowsBuilder;
        }

        [Inject]
        public IGameProgressKeeper ProgressKeeper { get; set; }
        
        [Inject]
        public IGameStageFactory StageFactory { get; set; }
        
        [Inject]
        public IStageConfigProvider StageConfigProvider { get; set; }
        
        [Inject]
        public IAvailableFeaturesProvider FeaturesProvider { get; set; }

        private GameStageView stageView;

        public void Start()
        {
           ShowHomeScreen();
        }

        private void ShowHomeScreen()
        {
            var home = windowsBuilder.CreateWindow<HomeWindow>();
            home.PlayRequested += StartCurrentStage;
        }
        
        private void StartCurrentStage()
        {
            stageView = Instantiate(stageViewPrefab, stageParent);
            var level = ProgressKeeper.CurrentLevel;
            var currentLevelConfig = StageConfigProvider.GetConfigFor(level);
            var features = FeaturesProvider.GetAvailableFeaturesAtLevel(level);
            var stage = StageFactory.PrepareStage(currentLevelConfig, features);
            
            stage.AllTasksComplete += OnAllStageTasksComplete;
            stage.MovesLimitReached += OnMovesLimitReached;
                
            stageView.InitWith(stage);
        }

        private void OnMovesLimitReached()
        {
            ShowLevelEndWindow<LevelFailedWindow>();
        }

        private void OnAllStageTasksComplete()
        {
            ProgressKeeper.CompleteCurrentLevel();
            ShowLevelEndWindow<LevelCompleteWindow>();
        }

        private void ShowLevelEndWindow<T>() where T : LevelEndWindow
        {
            Destroy(stageView.gameObject);
            
            var window = windowsBuilder.CreateWindow<T>();
            window.Init(ShowHomeScreen, StartCurrentStage);
        }

    }
}