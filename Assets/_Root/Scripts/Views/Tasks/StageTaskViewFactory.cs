namespace TestBlast.Views.Tasks
{
    using Field;
    using Meta;
    using UnityEngine;

    public class StageTaskViewFactory : MonoBehaviour
    {
        [SerializeField]
        private BlockTaskView blockTaskView;
        
        [SerializeField]
        private BlockViewFactory blockViewFactory;           

        public void PrepareTaskView(StageTask task, Transform parent)
        {
            if (!(task is CollectSimpleBlocksTask simpleBlocksTask))
            {
                //only simple tasks are currently supported
                return;
            }

            var view = Instantiate(blockTaskView, parent);
            view.InitWith(blockViewFactory, simpleBlocksTask);
        } 

    }
}