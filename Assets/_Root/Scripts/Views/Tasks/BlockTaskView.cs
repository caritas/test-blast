namespace TestBlast.Views.Tasks
{
    using Core.Field.Blocks;
    using Field;
    using Meta;
    using TMPro;
    using UnityEngine;

    public class BlockTaskView : TaskView
    {
        [SerializeField]
        private RectTransform blockParent;

        [SerializeField]
        private TMP_Text amountLeft;

        [SerializeField]
        private GameObject completed;

        [SerializeField]
        private GameObject counter;

        private CollectSimpleBlocksTask task;
        
        public void InitWith(BlockViewFactory blockViewFactory, CollectSimpleBlocksTask task)
        {
            this.task = task;
            var block = Instantiate(blockViewFactory.GetBlockViewPrefabFor(new SimpleBlock(task.Color)), blockParent);
            var rectTransform = block.GetComponent<RectTransform>();
            rectTransform.anchorMin = rectTransform.anchorMax = Vector2.one / 2;
            rectTransform.sizeDelta = blockParent.sizeDelta;
            
            amountLeft.SetText(task.Amount.ToString());
            
            task.TaskUpdated += OnTaskUpdated;
        }

        private void OnTaskUpdated()
        {
            if (task.Completed)
            {
                counter.SetActive(false);
                completed.SetActive(true);
                return;
            }
            
            amountLeft.SetText(task.Amount.ToString());
        }
    }
}