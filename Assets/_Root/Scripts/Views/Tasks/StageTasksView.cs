namespace TestBlast.Views.Tasks
{
    using Field;
    using Meta;
    using UnityEngine;

    public class StageTasksView : MonoBehaviour
    {
        [SerializeField]
        private StageTaskViewFactory taskViewFactory;
        
        public void InitFor(GameStage stage)
        {
            foreach (var task in stage.Tasks)
            {
                taskViewFactory.PrepareTaskView(task, transform);
            }
        }
    }
}