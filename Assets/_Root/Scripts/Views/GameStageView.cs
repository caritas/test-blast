namespace TestBlast.Views
{
    using System.Collections.Generic;
    using Core.Field.Blocks;
    using Field;
    using Meta;
    using Meta.Config;
    using Tasks;
    using Ui;
    using UnityEngine;
    using Zenject;

    public class GameStageView : MonoBehaviour
    {
        [SerializeField] 
        private GameFieldView fieldView;

        [SerializeField]
        private StageTasksView tasksView;

        [SerializeField]
        private MovesIndicator movesIndicator;           
        
        public void InitWith(GameStage stage)
        {
            fieldView.InitWith(stage.Field);
            
            movesIndicator.SubscribeTo(stage);
            
            tasksView.InitFor(stage);
        }
    }
}