namespace TestBlast.Core.Field
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Blocks;
    using Generators;
    using UnityEngine;

    public class GameField
    {
        private Cell[,] cells = new Cell[0,0];

        public int Width => cells.GetLength(1);
        
        public int Height => cells.GetLength(0);
        
        public IEnumerable<Cell> AllCells => cells.Cast<Cell>();

        public event Action<List<Block>> BlocksCleaned;

        public event Action MovePerformed;
        
        private IFieldView view;

        private IFieldGenerator generator;

        public GameField(IFieldGenerator generator)
        {
            this.generator = generator;
            cells = generator.GenerateField(this);
        }

        public Cell GetCellAtPosition(Position position)
        {
            if (position.Row < 0 || position.Row >= Height || position.Column < 0 || position.Column >= Width)
            {
                return null;
            }

            return cells[position.Row, position.Column];
        }
        
        public void SetView(IFieldView view)
        {
            this.view = view;
        }

        public async Task TryActivateCell(Position position)
        {
            var cell = GetCellAtPosition(position);
            if (cell == null)
            {
                return;
            }

            if (!await cell.TryActivate(this))
            {
                return;
            }
           
            PerformAvalanche();
            FillEmptyCells();
            
            MovePerformed?.Invoke();
        }

        public void DestroyBlocksInCells(List<Cell> cells)
        {
            var blocks = cells.Select(cell => cell.Block).ToList();
            foreach (var cell in cells)
            {
                cell.DestroyBlock();
            }
            BlocksCleaned?.Invoke(blocks);
        }

        private void PerformAvalanche()
        {
            for (var column = 0; column < Width; column++)
            {
                for (var row = Height - 1; row >= 0; row--)
                {
                    var nextCell = cells[row, column];
                    if (!nextCell.IsEmpty)
                    {
                        continue;
                    }

                    var lookUpRow = row;
                    do
                    {
                        lookUpRow--;
                    } while (!(lookUpRow == -1 || !cells[lookUpRow, column].IsEmpty));

                    if (lookUpRow == -1)
                    {
                        continue;
                    }

                    var fallenBlockOrigin = cells[lookUpRow, column];
                    nextCell.PeekBlockFrom(fallenBlockOrigin);
                    view.MoveBlock(fallenBlockOrigin, nextCell);
                    
                }
            }
        }

        private void FillEmptyCells()
        {
            for (var column = 0; column < Width; column++)
            {
                var cellsToAdd = 0;
                for (var row = 0; row < Height; row++)
                {
                    if (cells[row, column].IsEmpty)
                    {
                        cellsToAdd++;
                    }
                    else
                    {
                        break;
                    }
                }

                for (var i = 0; i < cellsToAdd; i++)
                {
                    var block = generator.GenerateNext();
                    var cell = cells[i, column];
                    cell.AddBlock(block);

                    view.PeekFallenBlock(cell, block);
                }
            }
        }

        public async Task CollectBlocksAndConvert(List<Cell> cells, Cell clickedCell, Block rowCleanerBlock)
        {
            var blocks = cells.Select(cell => cell.Block).ToList();
            blocks.Add(clickedCell.Block);
            foreach (var cell in cells)
            {
                cell.RemoveBlockSilently();
            }
            clickedCell.RemoveBlockSilently();
            
            BlocksCleaned?.Invoke(blocks);
            
            clickedCell.AddBlock(rowCleanerBlock);

            await view.CollectBlocksAndConvert(cells, clickedCell);
        }
    }
}