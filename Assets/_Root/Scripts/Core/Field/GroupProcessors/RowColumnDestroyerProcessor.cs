namespace TestBlast.Core.Field.GroupProcessors
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Blocks;
    using UnityEngine;

    public class RowColumnDestroyerProcessor : IBlockGroupProcessor
    {
        public Priority Priority => Priority.Simple;

        public bool CanProcess(List<Cell> cells)
        {
            return cells.Count >= 4;
        }

        public async Task Process(List<Cell> cells, Cell clickedCell, GameField field)
        {
            var newBlock = Random.value > .5? (Block) new RowCleanerBlock() : new ColumnCleanerBlock();
            await field.CollectBlocksAndConvert(cells.Except(new List<Cell> {clickedCell}).ToList(), clickedCell, newBlock);
        }
    }
}