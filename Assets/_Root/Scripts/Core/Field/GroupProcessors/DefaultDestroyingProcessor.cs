namespace TestBlast.Core.Field.GroupProcessors
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class DefaultDestroyingProcessor : IBlockGroupProcessor
    {
        public Priority Priority => Priority.Low;

        public bool CanProcess(List<Cell> cells)
        {
            return true;
        }

        public async Task Process(List<Cell> cells, Cell clickedCell, GameField field)
        {
            field.DestroyBlocksInCells(cells);
        }
    }
}