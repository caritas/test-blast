namespace TestBlast.Core.Field.GroupProcessors
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IBlockGroupProcessor
    {
        Priority Priority { get; }

        bool CanProcess(List<Cell> cells);
        
        Task Process(List<Cell> cells, Cell clickedCell, GameField field);
    }
}