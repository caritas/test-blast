namespace TestBlast.Core.Field
{
    using System.Threading.Tasks;

    public interface ICellView
    {
        Task DestroyBlock();
    }
}