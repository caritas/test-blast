namespace TestBlast.Core.Field
{
    using System.Threading.Tasks;
    using Blocks;
    using UnityEngine;

    public class Cell
    {
        public Position Position { get; }
        
        public Block Block { get; private set; }

        public bool IsEmpty => Block == null;

        private ICellView view;

        public Cell(Position position, Block block = null)
        {
            Position = position;
            Block = block;

            block?.SetCell(this);
        }

        public void InitView(ICellView view)
        {
            this.view = view;
        }

        public Task DestroyBlock()
        {
            Block = null;
            return view.DestroyBlock();
        }

        public void RemoveBlockSilently()
        {
            Block = null;
        }

        public async Task<bool> TryActivate(GameField gameField)
        {
            if (IsEmpty)
            {
                return false;
            }

            return await Block.TryActivate(gameField);
        }

        public void PeekBlockFrom(Cell cell)
        {
            Block = cell.Block;
            Block.SetCell(this);
            cell.Block = null;
        }

        public void AddBlock(Block block)
        {
            Block = block;
            block.SetCell(this);
        }
    }
}