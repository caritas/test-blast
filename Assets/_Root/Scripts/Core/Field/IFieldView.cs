namespace TestBlast.Core.Field
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Blocks;

    public interface IFieldView
    {
        Task MoveBlock(Cell from, Cell to);
        void PeekFallenBlock(Cell cell, Block block);
        Task CollectBlocksAndConvert(List<Cell> cells, Cell clickedCell);
    }
}