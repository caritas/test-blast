namespace TestBlast.Core.Field.Blocks
{
    using System.Linq;
    using System.Threading.Tasks;

    public class RowCleanerBlock : Block
    {
        public override async Task<bool> TryActivate(GameField gameField)
        {
            var row = gameField.AllCells
                .Where(cell => cell.Position.Row == Cell.Position.Row)
                //.Where(cell => cell != Cell)
                .ToList();

            gameField.DestroyBlocksInCells(row);
            return true;
        }
    }
    
    public class ColumnCleanerBlock : Block
    {
        public override async Task<bool> TryActivate(GameField gameField)
        {
            var row = gameField.AllCells
                .Where(cell => cell.Position.Column == Cell.Position.Column)
                //.Where(cell => cell != Cell)
                .ToList();

            gameField.DestroyBlocksInCells(row);
            return true;
        }
    }
}