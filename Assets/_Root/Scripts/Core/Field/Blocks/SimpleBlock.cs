namespace TestBlast.Core.Field.Blocks
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GroupProcessors;
    using UnityEngine;

    public class SimpleBlock : Block
    {
        public SimpleBlockColor Color { get; }
        
        private readonly List<IBlockGroupProcessor> processors = new List<IBlockGroupProcessor>()
        {
            new DefaultDestroyingProcessor()
        };

        public SimpleBlock(SimpleBlockColor color)
        {
            Color = color;
        }
        
        public void RegisterBlocksProcessor(IBlockGroupProcessor processor)
        {
            processors.Add(processor);
        }

        public override async Task<bool> TryActivate(GameField gameField)
        {
            //Simple block can be activated if it has at least one neighbour of the same color
            //We are making simple DFS to find group of cells with simple blocks of the same color
            var sameColorGroup = new HashSet<Cell>();
            var checkedGroup = new HashSet<Cell>();
            
            var openList = new Stack<Cell>();
            openList.Push(Cell);

            while (openList.Any())
            {
                var next = openList.Pop();
                if (checkedGroup.Contains(next))
                {
                    continue;
                }

                checkedGroup.Add(next);
                if (next.IsEmpty || !(next.Block is SimpleBlock simpleBlock) || simpleBlock.Color != Color)
                {
                    continue;
                }

                sameColorGroup.Add(next);
                    
                var neighbours = next.Position.AllNeighbours()
                    .Select(gameField.GetCellAtPosition)
                    .Where(c => c != null)
                    .ToList();

                foreach (var neighbour in neighbours)
                {
                    openList.Push(neighbour);
                }
            }

            if (sameColorGroup.Count > 1)
            {
                var cells = sameColorGroup.ToList();
                await processors
                    .OrderBy(processor => processor.Priority)
                    .First(groupProcessor => groupProcessor.CanProcess(cells))
                    .Process(cells, Cell, gameField);
                return true;
            }

            return false;
        }

    }

    public enum SimpleBlockColor
    {
        Blue, Green, Orange, Purple, Red, Yellow
    }
}