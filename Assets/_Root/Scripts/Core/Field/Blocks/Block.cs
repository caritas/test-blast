namespace TestBlast.Core.Field.Blocks
{
    using System.Threading.Tasks;

    public abstract class Block
    {
        public Cell Cell { get; private set; }
        
        public abstract Task<bool> TryActivate(GameField gameField);

        public void SetCell(Cell cell)
        {
            Cell = cell;
        }
    }
}