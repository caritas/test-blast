namespace TestBlast.Core.Field.Generators
{
    using System;
    using System.Collections.Generic;
    using Blocks;
    using GroupProcessors;

    public class RandomSimpleBlocksGenerator : IFieldGenerator
    {
        private readonly int width;
        
        private readonly int height;
        
        private readonly Random random;
        
        private List<SimpleBlockColor> allowedColors;

        private List<IBlockGroupProcessor> extraGroupProcessors;

        public RandomSimpleBlocksGenerator(int width, int height, List<SimpleBlockColor> allowedColors, int seed, List<IBlockGroupProcessor> extraGroupProcessors = null)
        {
            this.width = width;
            this.height = height;
            this.allowedColors = allowedColors;
            this.extraGroupProcessors = extraGroupProcessors ?? new List<IBlockGroupProcessor>();
            
            random = new Random(seed);
        }

        public Cell[,] GenerateField(GameField field)
        {
            var cells = new Cell[height, width];
            for (var row = 0; row < height; row++)
            {
                for (var column = 0; column < width; column++)
                {
                    var cell = new Cell(new Position(row, column), GenerateNext());
                    cells[row, column] = cell;
                }
            }

            return cells;
        }

        public Block GenerateNext()
        {
            var next = new SimpleBlock(allowedColors[random.Next(allowedColors.Count)]);
            extraGroupProcessors.ForEach(processor => next.RegisterBlocksProcessor(processor));
            return next;
        }
    }
}