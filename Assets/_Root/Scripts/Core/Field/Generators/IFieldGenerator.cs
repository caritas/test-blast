namespace TestBlast.Core.Field.Generators
{
    using Blocks;
    using JetBrains.Annotations;

    public interface IFieldGenerator
    {
        [NotNull] Cell[,] GenerateField(GameField field);

        Block GenerateNext();
    }
}