namespace TestBlast.Core.Field
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Working with X and Y can be little annoying because of different approaches to understand
    /// where each axis starts and in what order they exist in array. Explicit Row and Column can be better
    /// </summary>
    public struct Position
    {
        public int Row { get; }
        
        public int Column { get; }

        public Position(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool Equals(Position other)
        {
            return Row == other.Row && Column == other.Column;
        }

        public Position NeighbourAt(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return new Position(Row - 1, Column);
                case Direction.Down:
                    return new Position(Row + 1, Column);
                case Direction.Right:
                    return new Position(Row, Column + 1);
                case Direction.Left:
                    return new Position(Row, Column - 1);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, $"Unknown direction {direction}");
            }
        }

        public IEnumerable<Position> AllNeighbours()
        {
            yield return NeighbourAt(Direction.Up);
            yield return NeighbourAt(Direction.Right);
            yield return NeighbourAt(Direction.Down);
            yield return NeighbourAt(Direction.Left);
        }

        public override string ToString()
        {
            return $"[r:{Row}, c:{Column}]";
        }

        public override bool Equals(object obj)
        {
            return obj is Position other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Row * 397) ^ Column;
            }
        }

        public static bool operator ==(Position left, Position right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Position left, Position right)
        {
            return !left.Equals(right);
        }
    }
    
    public enum Direction
    {
        Up, Down, Right, Left
    }
}