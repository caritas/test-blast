namespace TestBlast.Core.Field
{
    public enum Priority
    {
        High = 1,
        Simple = 10,
        Low = 100
    }
}