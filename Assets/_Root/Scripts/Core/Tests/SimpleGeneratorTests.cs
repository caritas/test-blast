namespace TestBlast.Core.Field.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Blocks;
    using Generators;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class SimpleGeneratorTests
    {
        private IFieldGenerator generator;
        
        private GameField field;

        [SetUp]
        public void SetUp()
        {
            generator = new RandomSimpleBlocksGenerator(7, 5, new List<SimpleBlockColor>()
            {
                SimpleBlockColor.Blue,
                SimpleBlockColor.Green,
                SimpleBlockColor.Red, 
                SimpleBlockColor.Yellow,
                SimpleBlockColor.Orange,
                SimpleBlockColor.Purple
            }, 562);
            
            field = new GameField(generator);
            field.SetView(Substitute.For<IFieldView>());
        }

        [Test]
        public void _01ShouldCreateGeneratorWithoutErrors()
        {
            Assert.Pass();
        }
        
        [Test]
        public void _02FieldShouldHaveCorrectSize()
        {
            Assert.That(field.Width, Is.EqualTo(7));
            Assert.That(field.Height, Is.EqualTo(5));
        }

        [Test]
        public void _03FieldShouldNotHaveNulls()
        {
            CollectionAssert.AllItemsAreNotNull(field.AllCells);
        }

        [Test]
        public void _04FieldShouldNotHaveEmptyCells()
        {
            CollectionAssert.DoesNotContain(field.AllCells.Select(cell => cell.IsEmpty), true);
        }
    }
}