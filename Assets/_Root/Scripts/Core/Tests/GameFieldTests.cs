namespace TestBlast.Core.Field.Tests
{
    using System.Collections.Generic;
    using Blocks;
    using Generators;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class GameFieldTests
    {
        private GameField field;
        
        private RandomSimpleBlocksGenerator generator = new RandomSimpleBlocksGenerator(7, 5, new List<SimpleBlockColor>()
        {
            SimpleBlockColor.Blue,
            SimpleBlockColor.Green,
        }, 562);

        [SetUp]
        public void SetUp()
        {
            field = new GameField(generator);
        }
        
        [Test]
        public void _01ShouldCreateFieldWithoutErrors()
        {
            Assert.Pass();
        }
        
        [Test]
        public void _02ShouldPrepareFieldWithSpecifiedDimensions()
        {
            field.SetView(Substitute.For<IFieldView>());
            Assert.That(field.Width, Is.EqualTo(5));
            Assert.That(field.Height, Is.EqualTo(8));
        }

        [Test]
        public void _03ShouldReturnNull_WhenFetchingCellIsOutOfBounds()
        {
            field.SetView(Substitute.For<IFieldView>());
            
            Assert.That(field.GetCellAtPosition(new Position(5,7)), Is.Null);
            Assert.That(field.GetCellAtPosition(new Position(4,6)), Is.Not.Null);
        }

        class StubFieldGenerator : IFieldGenerator
        {
            private int width;
            
            private int height;

            public StubFieldGenerator(int width, int height)
            {
                this.width = width;
                this.height = height;
            }

            public Cell[,] GenerateField(GameField field)
            {
                var cells = new Cell[width,height];
                return cells;
            }

            public Block GenerateNext()
            {
                return null;
            }
        }
    }
}